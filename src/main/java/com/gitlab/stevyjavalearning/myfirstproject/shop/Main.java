package com.gitlab.stevyjavalearning.myfirstproject.shop;

import com.gitlab.stevyjavalearning.myfirstproject.shop.initdata.Init;
import com.gitlab.stevyjavalearning.myfirstproject.shop.view.UserView;

public class Main {
    public static void main(String[] args) {
        Init id = new Init();
        id.initData();
        UserView uv = new UserView();
        uv.commandLine();
    }
}
