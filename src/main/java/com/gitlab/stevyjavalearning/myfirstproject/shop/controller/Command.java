package com.gitlab.stevyjavalearning.myfirstproject.shop.controller;

public interface Command {
    boolean EXIT_COMMAND_LINE = false;
    boolean execute();
    String commandDetails();
    void setParent(Command command);
}
