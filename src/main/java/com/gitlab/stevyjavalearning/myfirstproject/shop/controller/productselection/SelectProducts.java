package com.gitlab.stevyjavalearning.myfirstproject.shop.controller.productselection;

import com.gitlab.stevyjavalearning.myfirstproject.shop.RamData;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.AbstractCommand;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Client;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Product;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SelectProducts extends AbstractCommand implements Command {
    @Override
    public boolean execute() {
        showProducts();
        selectedProducts();

        return !EXIT_COMMAND_LINE;
    }

    @Override
    public String commandDetails() {
        return "pls select products";
    }

    @Override
    public void setParent(Command command) {
        this.parent = command;
    }

    public void showProducts() {
        System.out.println("pls select products");
        for (int i = 0; i < RamData.getRamData().allProducts.size(); i++) {
            System.out.println(i);
            System.out.println(RamData.getRamData().allProducts.get(i));
        }
    }

    public Product selectedProducts() {
        Product product = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int selected = Integer.parseInt(reader.readLine());
            product = RamData.getRamData().allProducts.get(selected);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return product;
    }
}
