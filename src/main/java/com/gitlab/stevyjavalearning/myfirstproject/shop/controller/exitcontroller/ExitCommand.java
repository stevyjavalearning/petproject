package com.gitlab.stevyjavalearning.myfirstproject.shop.controller.exitcontroller;

import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.AbstractCommand;

public class ExitCommand extends AbstractCommand implements Command {

    @Override
    public boolean execute() {
        System.out.println("user bye");
        return EXIT_COMMAND_LINE;
    }

    @Override
    public String commandDetails() {
        return null;
    }

    @Override
    public void setParent(Command command) {
        this.parent = command;
    }
}

