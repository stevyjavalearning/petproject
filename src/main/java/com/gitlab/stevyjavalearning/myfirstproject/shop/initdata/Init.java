package com.gitlab.stevyjavalearning.myfirstproject.shop.initdata;

import com.gitlab.stevyjavalearning.myfirstproject.shop.RamData;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.*;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.product.Whiskey;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.product.Wine;

import java.util.ArrayList;
import java.util.List;

public class Init {
    public void initData() {
        List<Product> usersBasket = new ArrayList<>();
        Client client = new Client().setLogin("Stevy").setPassword("123456");
        RamData.getRamData().allClients.add(client);
        Group year2015 = new Group();
        year2015.setTitle("2015");

        Group year2019 = new Group();
        year2019.setTitle("2019");

        Group country = new Group();
        country.setTitle("Italia");

        year2015.setParentGroup(country);
        year2019.setParentGroup(country);

        Group japan = new Group();
        japan.setTitle("Japan");

        Group scotland = new Group();
        scotland.setTitle("Scotland");

        Group usa = new Group();
        usa.setTitle("United States");
        RamData.getRamData().allGroup.add(year2015);
        RamData.getRamData().allGroup.add(year2019);
        RamData.getRamData().allGroup.add(scotland);
        RamData.getRamData().allGroup.add(japan);
        RamData.getRamData().allGroup.add(country);
        //
        Whiskey whiskeyJapan = new Whiskey();
        whiskeyJapan.setTitle("Suntory Hakushu");
        whiskeyJapan.setWhiskeyExtract(8);
        whiskeyJapan.setGroup(japan);
        whiskeyJapan.setWhiskeyType(WhiskeyType.SINGLEMALT);
        whiskeyJapan.setPrice(9020);
        //

        Whiskey whiskeyJapan1 = new Whiskey();
        whiskeyJapan1.setTitle("Chichibu The First");
        whiskeyJapan1.setWhiskeyExtract(3);
        whiskeyJapan1.setGroup(japan);
        whiskeyJapan1.setWhiskeyType(WhiskeyType.SINGLEMALT);
        whiskeyJapan1.setPrice(12971);
        //

        Whiskey whiskeyScotland = new Whiskey();
        whiskeyScotland.setTitle("Macallan");
        whiskeyScotland.setWhiskeyExtract(12);
        whiskeyScotland.setGroup(scotland);
        whiskeyScotland.setWhiskeyType(WhiskeyType.SINGLEMALT);
        whiskeyScotland.setPrice(4370);
        //

        Whiskey whiskeyScotland1 = new Whiskey();
        whiskeyScotland1.setTitle("Lagavulin");
        whiskeyScotland1.setWhiskeyExtract(16);
        whiskeyScotland1.setGroup(scotland);
        whiskeyScotland1.setWhiskeyType(WhiskeyType.SINGLEMALT);
        whiskeyScotland1.setPrice(6620);
        //

        Whiskey whiskeyUSA = new Whiskey();
        whiskeyUSA.setTitle("Jack Daniels");
        whiskeyUSA.setGroup(usa);
        whiskeyUSA.setWhiskeyType(WhiskeyType.BLENDED);
        whiskeyUSA.setPrice(1851);
        //

        Whiskey whiskeyUSA1 = new Whiskey();
        whiskeyUSA1.setTitle("Maker's Mark");
        whiskeyUSA1.setGroup(usa);
        whiskeyUSA1.setWhiskeyType(WhiskeyType.BLENDED);
        whiskeyUSA1.setPrice(2526);
        //

        Wine redWineItaly = new Wine();
        redWineItaly.setTitle("Cecchi");
        redWineItaly.setGroup(year2015);
        redWineItaly.setType(WineType.RED);
        redWineItaly.setSugarContent(SugarContent.DRY);
        redWineItaly.setPrice(972);
        //

        Wine redWineItaly1 = new Wine();
        redWineItaly1.setTitle("Rondone");
        redWineItaly1.setGroup(year2019);
        redWineItaly1.setType(WineType.RED);
        redWineItaly1.setSugarContent(SugarContent.DRY);
        redWineItaly1.setPrice(921);
        //

        Wine whiteWineItaly = new Wine();
        whiteWineItaly.setTitle("Pffefferer");
        whiteWineItaly.setGroup(year2019);
        whiteWineItaly.setType(WineType.WHITE);
        whiteWineItaly.setSugarContent(SugarContent.DRY);
        whiteWineItaly.setPrice(1400);
        //

        Wine whiteWineItaly1 = new Wine();
        whiteWineItaly1.setTitle("Fontana");
        whiteWineItaly1.setGroup(year2015);
        whiteWineItaly1.setType(WineType.WHITE);
        whiteWineItaly1.setSugarContent(SugarContent.SEMIDRY);
        whiteWineItaly1.setPrice(1694);
        //
        RamData.getRamData().allProducts.add(whiskeyJapan);
        RamData.getRamData().allProducts.add(whiskeyJapan1);
        RamData.getRamData().allProducts.add(whiskeyScotland);
        RamData.getRamData().allProducts.add(whiskeyScotland1);
        RamData.getRamData().allProducts.add(whiskeyUSA);
        RamData.getRamData().allProducts.add(whiskeyUSA1);
        RamData.getRamData().allProducts.add(whiteWineItaly);
        RamData.getRamData().allProducts.add(whiteWineItaly1);
        RamData.getRamData().allProducts.add(redWineItaly);
        RamData.getRamData().allProducts.add(redWineItaly1);
    }
}

