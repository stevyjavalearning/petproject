package com.gitlab.stevyjavalearning.myfirstproject.shop.controller;

import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.calculatecontroller.CalculateCommand;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.defaultcontroller.DefaultCommand;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.exitcontroller.ExitCommand;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.logencontroller.LoginCommand;

public class CommandFactory {
    public Command getCommand(String commandType, Command parent) {
        if(commandType == null) {
            return new DefaultCommand();
        } else if (commandType.equals("exit")) {
            return new ExitCommand();
        } else if (commandType.equals("calculate")) {
            Command calculate = new CalculateCommand();
            calculate.setParent(parent);
            return calculate;
        } else if (commandType.equals("sign in")) {
            Command login = new LoginCommand();
            login.setParent(parent);
            return login;
        }  else {
            Command defaultCommand = new DefaultCommand();
            defaultCommand.setParent(parent);
            return defaultCommand;
        }
    }
}
