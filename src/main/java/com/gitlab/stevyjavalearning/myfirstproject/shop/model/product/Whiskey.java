package com.gitlab.stevyjavalearning.myfirstproject.shop.model.product;

import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Product;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.WhiskeyType;

public class Whiskey extends Product {
    private int whiskeyExtract;
    private WhiskeyType whiskeyType;

    public int getWhiskeyExtract() {
        return whiskeyExtract;
    }

    public void setWhiskeyExtract(int whiskeyExtract) {
        this.whiskeyExtract = whiskeyExtract;
    }

    public WhiskeyType getWhiskeyType() {
        return whiskeyType;
    }

    public void setWhiskeyType(WhiskeyType whiskeyType) {
        this.whiskeyType = whiskeyType;
    }

    @Override
    public String toString() {
        return "Whiskey{" +
                "whiskeyExtract=" + whiskeyExtract +
                ", whiskeyType=" + whiskeyType +
                '}';
    }

}
