package com.gitlab.stevyjavalearning.myfirstproject.shop.model;

public class Group {
    private Group parentGroup;
    private String title;
    private String description;

    public Group getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(Group parentGroup) {
        this.parentGroup = parentGroup;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Group{" +
                "parentGroup=" + parentGroup +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
