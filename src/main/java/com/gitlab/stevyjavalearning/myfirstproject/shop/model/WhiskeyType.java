package com.gitlab.stevyjavalearning.myfirstproject.shop.model;

public enum WhiskeyType {
    SINGLEMALT, BLENDED
}
