package com.gitlab.stevyjavalearning.myfirstproject.shop.model;

public enum WineType {
    WHITE, RED, SPARKING
}
