package com.gitlab.stevyjavalearning.myfirstproject.shop.model.product;

import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Product;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.SugarContent;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.WineType;

public class Wine extends Product {
    private Integer alc;
    private String toninity;
    private WineType type;
    private SugarContent sugarContent;

    public SugarContent getSugarContent() {
        return sugarContent;
    }

    public void setSugarContent(SugarContent sugarContent) {
        this.sugarContent = sugarContent;
    }

    public Integer getAlc() {
        return alc;
    }

    public void setAlc(Integer alc) {
        this.alc = alc;
    }

    public String getToninity() {
        return toninity;
    }

    public void setToninity(String toninity) {
        this.toninity = toninity;
    }

    public WineType getType() {
        return type;
    }

    public void setType(WineType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Wine{" +
                "alc=" + alc +
                ", toninity='" + toninity + '\'' +
                ", type=" + type +
                ", sugarContent=" + sugarContent +
                '}';
    }
}
