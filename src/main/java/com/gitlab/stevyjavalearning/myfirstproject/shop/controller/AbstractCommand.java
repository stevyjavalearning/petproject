package com.gitlab.stevyjavalearning.myfirstproject.shop.controller;


import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Client;


public abstract class AbstractCommand {
    protected Client owner;
    protected Command parent;

    public Command getParent() {
        return parent;
    }
}
