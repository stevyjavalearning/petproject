package com.gitlab.stevyjavalearning.myfirstproject.shop.model;

public class Client {
    private String name;
    private String email;
    private int phone;
    private int age;
    private Address address;
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public Client setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Client setPassword(String password) {
        this.password = password;
        return this;
    }

    private void createBasket() {
        Basket basketClient = new Basket();
        basketClient.setBasketOwner(this);
    }

    public Client() {
        createBasket();
    }

    public String getName() {
        return name;
    }

    public Client setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Client setEmail(String email) {
        this.email = email;
        return this;
    }

    public int getPhone() {
        return phone;
    }

    public Client setPhone(int phone) {
        this.phone = phone;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Client setAge(int age) {
        this.age = age;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public Client setAddress(Address address) {
        this.address = address;
        return this;
    }
}
