package com.gitlab.stevyjavalearning.myfirstproject.shop.view;



import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.CommandFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserView {
    Command parentCommand = null;
    public void commandLine() {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        try {
            boolean flag = true;
            CommandFactory commandFactory = new CommandFactory();
            System.out.println("hello, this is shop.");
            System.out.println("login and password please");
            while (flag) {
                String command = rd.readLine();
                if (command.contains("help")) {
                    Command newCommand = commandFactory.getCommand(command.split(" ")[0], parentCommand);
                    parentCommand = newCommand;
                    System.out.println(newCommand.commandDetails());
                } else {
                    Command newCommand = commandFactory.getCommand(command, parentCommand);
                    parentCommand = newCommand;
                    flag = newCommand.execute();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
