package com.gitlab.stevyjavalearning.myfirstproject.shop.controller.defaultcontroller;

import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.AbstractCommand;

public class DefaultCommand extends AbstractCommand implements Command {

    @Override
    public boolean execute() {
        System.out.println("Я не знаю что за команда.");
        return !EXIT_COMMAND_LINE;
    }

    @Override
    public String commandDetails() {
        return "This is unknown command.";
    }

    @Override
    public void setParent(Command command) {
        this.parent = command;
    }
}

