package com.gitlab.stevyjavalearning.myfirstproject.shop.model;

public enum SugarContent {
    DRY, SEMIDRY, SEMISWEET, SWEET
}

