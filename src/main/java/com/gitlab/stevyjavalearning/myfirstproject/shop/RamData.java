package com.gitlab.stevyjavalearning.myfirstproject.shop;

import com.gitlab.stevyjavalearning.myfirstproject.shop.model.*;

import java.util.ArrayList;
import java.util.List;

public class RamData {
    private static RamData ramData = new RamData();
    private RamData() {}
    public static RamData getRamData() {
        return ramData;
    }
    public List<Client> allClients = new ArrayList<>();
    public List<Product> allProducts = new ArrayList<>();
    public List<Address> allAddress = new ArrayList<>();
    public List<Basket> allBasket = new ArrayList<>();
    public List<Group> allGroup = new ArrayList<>();
    public List<CheckProduct> allCheckProducts = new ArrayList<>();
}
