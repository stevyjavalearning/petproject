package com.gitlab.stevyjavalearning.myfirstproject.shop.model;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    private Client basketOwner;
    private List<Product> basketList = new ArrayList<>();

    public Client getBasketOwner() {
        return basketOwner;
    }

    public void setBasketOwner(Client basketOwner) {
        this.basketOwner = basketOwner;
    }

    public List<Product> getBasketList() {
        return basketList;
    }

    public void setBasketList(List<Product> basketList) {
        this.basketList = basketList;
    }
}
