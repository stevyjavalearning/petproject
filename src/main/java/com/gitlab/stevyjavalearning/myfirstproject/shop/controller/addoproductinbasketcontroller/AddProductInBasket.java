package com.gitlab.stevyjavalearning.myfirstproject.shop.controller.addoproductinbasketcontroller;

import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.AbstractCommand;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Product;

import java.util.ArrayList;

public class AddProductInBasket extends AbstractCommand implements Command {
    @Override
    public boolean execute() {
        addProductInBasket();
        //Добавить аргумент products через View, из команды SelectProducts
        //hello
        return !EXIT_COMMAND_LINE;
    }

    @Override
    public String commandDetails() {
        return "This command add your products in basket";
    }

    @Override
    public void setParent(Command command) {
        this.parent = command;
    }

    public ArrayList<Product> addProductInBasket(Product product) {
        ArrayList<Product> clientBasket = new ArrayList<>();
        clientBasket.add(product);
        return clientBasket;
    }
}
