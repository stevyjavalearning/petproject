package com.gitlab.stevyjavalearning.myfirstproject.shop.controller.calculatecontroller;

import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.AbstractCommand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class CalculateCommand extends AbstractCommand implements Command {

    @Override
    public boolean execute() {
        calculate(convertToReversePolish(buffer()));
        return !EXIT_COMMAND_LINE;
    }

    @Override
    public String commandDetails() {
        return "This is command of calculate.";
    }

    private String buffer() {
        BufferedReader calculateNumber = new BufferedReader(new InputStreamReader(System.in));
        String mathematicalExample = "";
        try {
            mathematicalExample = calculateNumber.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mathematicalExample;
    }


    private void calculate(String res) {
        Stack<Integer> number = new Stack<>();
        String reversPolish;
        String[] arrayPolish;
        int q;
        int operandA;
        int operandB;
        reversPolish = res;

        arrayPolish = reversPolish.split(" ");
        for (String numberAndOperators : arrayPolish) {
            try {
                int numberForStack = Integer.parseInt(numberAndOperators);
                number.push(numberForStack);
            } catch (NumberFormatException e) {
                if (number.isEmpty()) {
                    break;
                } else {
                    operandB = number.pop();
                    if (number.isEmpty()) {
                        break;
                    } else {
                        operandA = number.pop();
                        switch (numberAndOperators) {
                            case "+":
                                number.push(operandA + operandB);
                                break;
                            case "-":
                                number.push(operandA - operandB);
                                break;
                            case "*":
                                number.push(operandA * operandB);
                                break;
                            case "/":
                                number.push(operandA / operandB);
                                break;
                        }
                    }
                }
            }
        }
        q = number.peek();
        System.out.println(q);
    }


    private String convertToReversePolish(String exp) {
        if (exp == null)
            return null;
        String res = "";
        int len = exp.length();
        Stack<Character> operator = new Stack<>();
        Stack<String> reversePolish = new Stack<>();
        //avoid checking empty
        operator.push('#');
        for (int i = 0; i < len; ) {
            //deal with space
            while (i < len && exp.charAt(i) == ' ')
                i++;
            if (i == len)
                break;
            //if is number
            if (isNum(exp.charAt(i))) {
                String num = "";
                while (i < len && isNum(exp.charAt(i)))
                    num += exp.charAt(i++);
                reversePolish.push(num);
                //is operator
            } else if (isOperator(exp.charAt(i))) {
                char op = exp.charAt(i);
                switch (op) {
                    case '(':
                        operator.push(op);
                        break;
                    case ')':
                        while (operator.peek() != '(')
                            reversePolish.push(Character.toString(operator.pop()));
                        operator.pop();
                        break;
                    case '+':
                    case '-':
                        if (operator.peek() == '(')
                            operator.push(op);
                        else {
                            while (operator.peek() != '#' && operator.peek() != '(')
                                reversePolish.push(Character.toString(operator.pop()));
                            operator.push(op);
                        }
                        break;
                    case '*':
                    case '/':
                        if (operator.peek() == '(')
                            operator.push(op);
                        else {
                            while (operator.peek() != '#' && operator.peek() != '+' &&
                                    operator.peek() != '-' && operator.peek() != '(')
                                reversePolish.push(Character.toString(operator.pop()));
                            operator.push(op);
                        }
                        break;
                }
                i++;
            }
        }
        while (operator.peek() != '#')
            reversePolish.push(Character.toString(operator.pop()));
        while (!reversePolish.isEmpty())
            res = res.length() == 0 ? reversePolish.pop() + res : reversePolish.pop() + " " + res;
        return res;
    }

    public boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')';
    }

    public boolean isNum(char c) {
        return c - '0' >= 0 && c - '0' <= 9;
    }

    @Override
    public void setParent(Command command) {
        this.parent = command;
    }
}

