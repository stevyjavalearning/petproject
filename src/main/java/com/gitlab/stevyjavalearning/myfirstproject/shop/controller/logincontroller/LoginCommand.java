package com.gitlab.stevyjavalearning.myfirstproject.shop.controller.logincontroller;

import com.gitlab.stevyjavalearning.myfirstproject.shop.RamData;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.Command;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.AbstractCommand;
import com.gitlab.stevyjavalearning.myfirstproject.shop.controller.productselection.SelectProducts;
import com.gitlab.stevyjavalearning.myfirstproject.shop.model.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LoginCommand extends AbstractCommand implements Command {
    private String login;
    private String password;

    public Client loginAndPassword() {
        System.out.println("Login pls");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            login = reader.readLine();
            System.out.println("Password pls");
            password = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Client client : RamData.getRamData().allClients) {
            if(login.equals(client.getLogin()) && password.equals(client.getPassword())) {
                this.owner = client;
                System.out.println("Верно");
            } else {
                System.out.println("Пароль не верен.");
                loginAndPassword();
            }

        }

        return owner;
    }

    @Override
    public boolean execute() {
        loginAndPassword();
        return !EXIT_COMMAND_LINE;
    }

    @Override
    public String commandDetails() {
        return null;
    }

    @Override
    public void setParent(Command command) {
        this.parent = command;
    }
}
