package commandline;

public class ExitCommand implements Command {

    @Override
    public boolean execute() {
        System.out.println("user bye");
        return false;
    }
}
