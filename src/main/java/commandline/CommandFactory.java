package commandline;

public class CommandFactory {
    public Command getCommand(String commandType) {
        if(commandType == null) {
            return new DefaultCommand();
        } else if (commandType.equals("exit")) {
            return new ExitCommand();
        } else if (commandType.equals("calculate")) {
            return new CalculateCommand();
        } else {
            return new DefaultCommand();
        }
    }
}
