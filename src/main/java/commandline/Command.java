package commandline;

public interface Command {
    boolean execute();
}
