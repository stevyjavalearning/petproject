package commandline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainCommandLine {
    public void commandLine() {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        try {
            boolean flag = true;
            CommandFactory commandFactory = new CommandFactory();
            while (flag) {
                String command = rd.readLine();
                flag = commandFactory.getCommand(command).execute();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

